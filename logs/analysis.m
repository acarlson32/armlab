%% Init
close all
clear all
clc
delimiter = ',';
startRow = 2;
formatSpec = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

% Unambiguous colors for those who may be color blind
colors = [[0.35, 0.7, 0.9]; [0, 0.6, 0.5]; [0.9, 0.6, 0]; [0.8, 0.6, 0.7]; [0.8, 0.4, 0]];


%% 3.1 Teach and Repeat
filename = '/home/smess/courses/rob550/lab3/armlab/logs/3.1-tr-11-points.csv';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
tr = [dataArray{1:end-1}];

figure(2)
plot3(tr(:,6), tr(:,7), tr(:,8))  
xlabel('X (millimeters)')
ylabel('Y (millimeters)')
zlabel('Z (millimeters)')

figure(8)
hold on
Y = tr(:,1) - min(tr(:,1));
X = tr(:,2:5) .* 180 / pi;
Xr = tr(:,10:13) .* 180 / pi;
for i = 1:4
    plot(Y, X(:,i), '.:', 'Color', colors(i,:))
end
for i = 1:4
    plot(Y, Xr(:,i), 'Color', colors(i,:))
end
[peaks, locs] = findpeaks(-Xr(:,3));
locs(find(peaks < 10)) = [];
for i = 1:length(locs)
    plot([Y(locs(i)), Y(locs(i))], [-160, 20], ':r', 'LineWidth', 1)
end
ylabel('Joint Angle (degrees)')
xlabel('Time (seconds)')
legend('Base Joint', 'Shoulder Joint', 'Elbow Joint', 'Wrist Joint', 'Base Desired', 'Shoulder Desired', 'Elbow Desired', 'Wrist Desired', 'Waypoint Configurations')
hold off


%% 3.2 Linear
filename = '/home/smess/courses/rob550/lab3/armlab/logs/3.2-linear-11-points.csv';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
linear = [dataArray{1:end-1}];

figure()
plot3(linear(:,6), linear(:,7), linear(:,8))  

figure()
hold on
Y = linear(:,1) - min(linear(:,1));
X = linear(:,2:5) .* 180 / pi;
Xr = linear(:,10:13) .* 180 / pi;
for i = 1:4
    plot(Y, X(:,i), '.:', 'Color', colors(i,:))
end
for i = 1:4
    plot(Y, Xr(:,i), 'Color', colors(i,:))
end
[peaks, locs] = findpeaks(-Xr(:,2));
locs(find(peaks < 10)) = [];
for i = 1:length(locs)
    plot([Y(locs(i)), Y(locs(i))], [-160, 20], ':r', 'LineWidth', 1)
end
ylabel('Joint Angle (degrees)')
xlabel('Time (seconds)')
legend('Base Joint', 'Shoulder Joint', 'Elbow Joint', 'Wrist Joint', 'Base Desired', 'Shoulder Desired', 'Elbow Desired', 'Wrist Desired', 'Waypoint Configurations')
hold off


%% 3.3 Cubic Spline
filename = '/home/smess/courses/rob550/lab3/armlab/logs/3.3-cubic-spline-11-points.csv';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
cubic = [dataArray{1:end-1}];

figure()
plot3(cubic(:,6), cubic(:,7), cubic(:,8)) 

figure()
hold on
Y = cubic(:,1) - min(cubic(:,1));
X = cubic(:,2:5) .* 180 / pi;
Xr = cubic(:,10:13) .* 180 / pi;
for i = 1:4
    plot(Y, X(:,i), '.:', 'Color', colors(i,:))
end
for i = 1:4
    plot(Y, Xr(:,i), 'Color', colors(i,:))
end
[peaks, locs] = findpeaks(-Xr(:,3));
locs(find(peaks < 10)) = [];
for i = 1:length(locs)
    plot([Y(locs(i)), Y(locs(i))], [-160, 20], ':r', 'LineWidth', 1)
end
ylabel('Joint Angle (degrees)')
xlabel('Time (seconds)')
legend('Base Joint', 'Shoulder Joint', 'Elbow Joint', 'Wrist Joint', 'Base Desired', 'Shoulder Desired', 'Elbow Desired', 'Wrist Desired', 'Waypoint Configurations')
hold off

figure()
hold on
Y = cubic(:,1) - min(cubic(:,1));
X = cubic(:,2:5) .* 180 / pi;
Xr = cubic(:,10:13) .* 180 / pi;
for i = 1:4
    plot(Y, X(:,i), '.:', 'Color', colors(i,:))
end
for i = 1:4
    plot(Y, Xr(:,i), 'Color', colors(i,:))
end
[peaks, locs] = findpeaks(-Xr(:,3));
locs(find(peaks < 10)) = [];
for i = 1:length(locs)
    plot([Y(locs(i)), Y(locs(i))], [-160, 20], ':r', 'LineWidth', 1)
end
ylabel('Joint Angle (degrees)')
xlabel('Time (seconds)')
legend('Base Joint', 'Shoulder Joint', 'Elbow Joint', 'Wrist Joint', 'Base Desired', 'Shoulder Desired', 'Elbow Desired', 'Wrist Desired', 'Waypoint Configurations')
hold off


%% 4.3 Lemniscate
filename = '/home/smess/courses/rob550/lab3/armlab/logs/4.3-lemniscate.csv';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
lemniscate = [dataArray{1:end-1}];

figure()
plot3(lemniscate(:,6), lemniscate(:,7), lemniscate(:,8))  
xlabel('X (millimeters)')
ylabel('Y (millimeters)')
zlabel('Z (millimeters)')

%% 4.3 Lemniscate 3D
filename = '/home/smess/courses/rob550/lab3/armlab/logs/4.3-lemniscate-3d.csv';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
lemniscate3 = [dataArray{1:end-1}];

figure()
plot3(lemniscate3(:,6), lemniscate3(:,7), lemniscate3(:,8))   
xlabel('X (millimeters)')
ylabel('Y (millimeters)')
zlabel('Z (millimeters)')