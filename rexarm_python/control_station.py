import sys
import cv2
import numpy as np
import math
from math import cos, sin
from PyQt4 import QtGui, QtCore, Qt
from ui import Ui_MainWindow
from rexarm import Rexarm

from video import Video
import time
import csv

""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
R2D = 180.0/3.141592

""" Pyxel Positions of image in GUI """
MIN_X = 310
MAX_X = 950

MIN_Y = 30
MAX_Y = 510

WPT_THRES = 0.1#0.075
WPT_KP = -10
LINK_LENGTH = [99.8, 98, 110]
X_OFFSET = 20
Z_OFFSET = 115

class Gui(QtGui.QMainWindow):
    """ 
    Main GUI Class
    It contains the main function and interfaces between 
    the GUI and functions
    """
    def __init__(self,parent=None):
    	# Teach and repeat: waypoint following variables
    	self.wayPoint_list = []
	self.velocity_list = []
	self.doPlayback = False
	self.do_learn = False
	self.record_init_files=0
	self.save_waypoints=1
	self.actualJointPoint_list=[]
	self.actualJointPoint_timestamp=[]
	self.f_gt_waypoints = -1 
        self.f_joint_feedback = -1 
        self.f_joint_feedback_timestamp = -1
	self.last_waypoint=time.time()
        self.interp_flag = "cubic"
	self.worldEffector = [0, 0, 0, 0]
	self.define_template_train = 0
	self.template_coords = [[0,0] for x in range(4)]
	self.template_board_coords = [[0, 0] for x in range(4)]
	self.template_click_id = 0

        QtGui.QWidget.__init__(self,parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
	
        """ Main Variables Using Other Classes"""
        self.rex = Rexarm()
        self.video = Video(cv2.VideoCapture(0))

        """ Other Variables """
        self.last_click = np.float32([0,0])

        """ Set GUI to track mouse """
        QtGui.QWidget.setMouseTracking(self,True)

        """ 
        Video Function 
        Creates a timer and calls play() function 
        according to the given time delay (27mm) 
        """
        self._timer = QtCore.QTimer(self)
        self._timer.timeout.connect(self.play)
        self._timer.start(27)
       
        """ 
        LCM Arm Feedback
        Creates a timer to call LCM handler continuously
        No delay implemented. Reads all time 
        """  
        self._timer2 = QtCore.QTimer(self)
        self._timer2.timeout.connect(self.rex.get_feedback)
        self._timer2.start(5)

        """ 
        Connect Sliders to Function
        """ 
        self.ui.sldrBase.valueChanged.connect(self.slider_change)
        self.ui.sldrShoulder.valueChanged.connect(self.slider_change)
        self.ui.sldrElbow.valueChanged.connect(self.slider_change)
        self.ui.sldrWrist.valueChanged.connect(self.slider_change)
        self.ui.sldrSpeed.valueChanged.connect(self.slider_change)
        self.ui.sldrMaxTorque.valueChanged.connect(self.slider_change)

        """ Commands the arm as the arm initialize to 0,0,0,0 angles """
        self.slider_change() 
        
        """ Connect Buttons to Functions """
        self.ui.btnLoadCameraCal.clicked.connect(self.load_camera_cal)
        self.ui.btnPerfAffineCal.clicked.connect(self.affine_cal)
        self.ui.btnTeachRepeat.clicked.connect(self.tr_initialize)
        self.ui.btnAddWaypoint.clicked.connect(self.tr_add_waypoint)
        self.ui.btnSmoothPath.clicked.connect(self.tr_smooth_path)
        self.ui.btnPlayback.clicked.connect(self.tr_playback)
        self.ui.btnLoadPlan.clicked.connect(self.load_plan)
        self.ui.btnDefineTemplate.clicked.connect(self.def_template)
        self.ui.btnLocateTargets.clicked.connect(self.template_match)
        self.ui.btnExecutePath.clicked.connect(self.exec_path)

	# Initial Configurations and Loads
	#self.load_camera_cal()


    def play(self):
        """ 
        Play Funtion
        Continuously called by GUI 
        """
	if (self.ui.chkRecord.isChecked()):
	    self.record()
	if (self.do_learn and ((time.time() - self.last_waypoint) > 1.)):
	   self.tr_add_waypoint()
	   self.last_waypoint=time.time()
	if (self.doPlayback):
	    self.playback()

	self.forward_kin()
	#self.path_lemniscate()
	#self.path_butterfly()
	#self.path_lissajous()
	#self.path_sophisticated()
	#pt = self.inv_kin_touch(100,200)
        #self.ui.sldrBase.setValue(pt[0] * R2D)
        #self.ui.sldrShoulder.setValue(pt[1] * R2D)
        #self.ui.sldrElbow.setValue(pt[2] * R2D)
        #self.ui.sldrWrist.setValue(pt[3] * R2D)
        #self.slider_change()
	#self.goto_point(-250,100,30,-30)
	#self.path_from_template_match()

	""" Renders the Video Frame """
        try:
            self.video.captureNextFrame()
	    if (self.ui.thresImage.isChecked()):
	        frame = self.video.convertFrameThres() 
	    else:
		frame = self.video.convertFrameRgb()
            self.ui.videoFrame.setPixmap(frame)
            self.ui.videoFrame.setScaledContents(True)
        except TypeError as e:
            print "No frame", e
        
        """ 
        Update GUI Joint Coordinates Labels
        TO DO: include the other slider labels 
        """
        self.ui.rdoutBaseJC.setText(str(self.rex.joint_angles_fb[0]*R2D))
        self.ui.rdoutShoulderJC.setText(str(self.rex.joint_angles_fb[1]*R2D))
        self.ui.rdoutElbowJC.setText(str(self.rex.joint_angles_fb[2]*R2D))
        self.ui.rdoutWristJC.setText(str(self.rex.joint_angles_fb[3]*R2D))

	self.ui.rdoutX.setText(str(self.worldEffector[0]))
	self.ui.rdoutY.setText(str(self.worldEffector[1]))
	self.ui.rdoutZ.setText(str(self.worldEffector[2]))
	self.ui.rdoutT.setText(str(self.worldEffector[3] * R2D))

        """ 
        Mouse position presentation in GUI
        """    
        x = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).x()
        y = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).y()
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)):
            self.ui.rdoutMousePixels.setText("(-,-)")
            self.ui.rdoutMouseWorld.setText("(-,-)")
        else:
            x = x - MIN_X
            y = y - MIN_Y
            self.ui.rdoutMousePixels.setText("(%.0f,%.0f)" % (x,y))
            if (self.video.aff_flag == 2):
                [wx,wy] =self.pixel_to_mm(x,y)
                self.ui.rdoutMouseWorld.setText("(%.0f,%.0f)" % (wx,wy))
            else:
                self.ui.rdoutMouseWorld.setText("(-,-)")

        """ 
        Updates status label when rexarm playback is been executed.
        This will be extended to includ eother appropriate messages
        """ 
        if(self.rex.plan_status == 1):
            self.ui.rdoutStatus.setText("Playing Back - Waypoint %d"
                                    %(self.rex.wpt_number + 1))

    def pixel_to_mm(self,x,y):
        homocoord = np.array([0,0,1])
        transform_mat = np.vstack((self.video.aff_matrix,homocoord))
        xy_hc = np.array([x,y,1])
        w_hc = np.dot(transform_mat,xy_hc)

	return [w_hc[0], w_hc[1]]

    def slider_change(self):
        """ 
        Function to change the slider labels when sliders are moved
        and to command the arm to the given position 
        """
        self.ui.rdoutBase.setText(str(self.ui.sldrBase.value()))
        self.ui.rdoutShoulder.setText(str(self.ui.sldrShoulder.value()))
        self.ui.rdoutElbow.setText(str(self.ui.sldrElbow.value()))
        self.ui.rdoutWrist.setText(str(self.ui.sldrWrist.value()))
        self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()))

        self.ui.rdoutTorq.setText(str(self.ui.sldrMaxTorque.value()) + "%")
        self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
	if (self.doPlayback == False):
	    for i in range(4):
                self.rex.speed[i] = self.ui.sldrSpeed.value()/100.0
        self.rex.joint_angles[0] = self.ui.sldrBase.value()*D2R
        self.rex.joint_angles[1] = self.ui.sldrShoulder.value()*D2R
        self.rex.joint_angles[2] = self.ui.sldrElbow.value()*D2R
        self.rex.joint_angles[3] = self.ui.sldrWrist.value()*D2R

        self.rex.cmd_publish()

    def mousePressEvent(self, QMouseEvent):
        """ 
        Function used to record mouse click positions for 
        affine calibration 
        """
 
        """ Get mouse posiiton """
        x = QMouseEvent.x()
        y = QMouseEvent.y()

        """ If mouse position is not over the camera image ignore """
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)): 
            return

        """ Change coordinates to image axis """
        self.last_click[0] = x - MIN_X
        self.last_click[1] = y - MIN_Y

	if (self.define_template_train == True):
	    if self.template_click_id < 4:
	    	self.template_coords[self.template_click_id] = [(x-MIN_X), (y-MIN_Y)]
		self.ui.rdoutStatus.setText("Template Calibration: Click template bounding box, point %d"
		              		    %(self.template_click_id + 1))
	    else:
		self.template_board_coords[self.template_click_id - 4] = [(x-MIN_X), (y-MIN_Y)]
		self.ui.rdoutStatus.setText("Template Calibration: Click board bounding box, point %d"
		              		    %(self.template_click_id + 1))
	    self.template_click_id += 1
	    if (self.template_click_id == 8):
		self.template_click_id = 0
                self.ui.rdoutStatus.setText("Waiting for input")
		self.define_template_train = False
		self.template_calculate()
       
        """ If affine calibration is been performed """
        if (self.video.aff_flag == 1):
            """ Save last mouse coordinate """
            self.video.mouse_coord[self.video.mouse_click_id] = [(x-MIN_X),
                                                                 (y-MIN_Y)]

            """ Update the number of used poitns for calibration """
            self.video.mouse_click_id += 1

            """ Update status label text """
            self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                      %(self.video.mouse_click_id + 1))

            """ 
            If the number of click is equal to the expected number of points
            computes the affine calibration.
            TO DO: Change this code to use you programmed affine calibration
            and NOT openCV pre-programmed function as it is done now.
            """
            if(self.video.mouse_click_id == self.video.aff_npoints):
                """ 
                Update status of calibration flag and number of mouse
                clicks
                """
                self.video.aff_flag = 2
                self.video.mouse_click_id = 0
                
                """ Perform affine calibration with OpenCV """
                """
                self.video.aff_matrix = cv2.getAffineTransform(
                                        self.video.mouse_coord,
                                        self.video.real_coord)
                """
                """ Team 2 calibration function """
                list_mouse_pts = self.collect_mousepts_for_affine_calib()
                list_world_pts = self.collect_world_pts()
                self.video.aff_matrix = self.our_affine_calibration_fun(list_mouse_pts,list_world_pts)

                """ Updates Status Label to inform calibration is done """ 
                self.ui.rdoutStatus.setText("Waiting for input")

                """ 
                Uncomment to gether affine calibration matrix numbers 
                on terminal
                """ 


    def collect_mousepts_for_affine_calib(self):
        list_mouse_pts=[]
        #list_mouse_pts.append(self.last_click)
        list_mouse_pts=self.video.mouse_coord

        return list_mouse_pts

    def collect_world_pts(self):
        list_world_pts=self.video.real_coord

        return list_world_pts

    def our_affine_calibration_fun(self,list_mouse_pts, list_world_pts):
        A=[]
        b=[]
        # affine calibration
        for px,py in list_mouse_pts:
            A.append([px, py, 1, 0, 0, 0])
            A.append([0, 0, 0, px, py, 1])
        for wx,wy in list_world_pts:
            b.append([wx])
            b.append([wy])
        A = np.array(A)
        b = np.array(b)

	abcdef = np.linalg.lstsq(A, b)[0]

	f = open('affine_calib.csv', 'w')
	np.savetxt(f, abcdef, delimiter=',')
	f.close()

        return abcdef.reshape((2,3))

    def affine_cal(self):
        """ 
        Function called when affine calibration button is called.
        Note it only chnage the flag to record the next mouse clicks
        and updates the status text label 
        """
        self.video.aff_flag = 1 
        self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                    %(self.video.mouse_click_id + 1))

    def load_camera_cal(self):
        self.video.loadCalibration()
        print "Load Camera Cal"

    def tr_initialize(self):
        print "Teach and Repeat"
        self.wayPoint_list=[]
        self.ui.sldrMaxTorque.setValue(0)
	self.do_learn =True

    def tr_add_waypoint(self):
	waypoint = [0, 0, 0, 0, 0]
	for i in range(4):
	    waypoint[i] = self.rex.joint_angles_fb[i]
        self.wayPoint_list.append(waypoint)
        print "Add Waypoint"
	print "Waypoint List: "
	lol(self.wayPoint_list)

    def path_lemniscate(self):
	t = time.time() # * 2.5
	a = 50
	x = (a * cos(t)) / (1 + sin(t) ** 2) + 90
	y = (a * sin(t) * cos(t)) / (1 + sin(t) ** 2) + 90
	z = 0.5 * (a * cos(t)) / (1 + sin(t) ** 2) + 85
	#z = 85
	self.goto_point(x, y, z, -90)

    def path_butterfly(self):
	t = time.time()/2.
	a = 8
	x = a * sin(t)*( math.exp(cos(t)) - 2.*cos(4.*t) - sin(t/12.)**5) + 80
	y = a * cos(t)*( math.exp(cos(t)) - 2.*cos(4.*t) - sin(t/12.)**5) + 80
	z = -78
	self.goto_point(x,y,z,-90)

    def path_lissajous(self):
	t = time.time() / 5.
	a = 30
	x = a * cos(4*t) + 75
	y = a * cos(9*t) + 75
	z = -78
	self.goto_point(x,y,z,-90)
 
    def path_sophisticated(self):
	t = time.time()
	scale = 20
	k = 0.65
	b = 1.
	a = k * b
	x = scale * ((a - b) * cos(t) + b * cos(t * (a/b - 1))) + 80
	y = scale * ((a - b) * sin(t) - b * sin(t * (a/b - 1))) + 80
	z = -78
	self.goto_point(x,y,z,-90)
	
    def goto_point(self, x, y, z, approach_angle):
        pt = self.inv_kin(x, y, z, approach_angle)
	self.ui.sldrBase.setValue(pt[0] * R2D)
        self.ui.sldrShoulder.setValue(pt[1] * R2D)
        self.ui.sldrElbow.setValue(pt[2] * R2D)
        self.ui.sldrWrist.setValue(pt[3] * R2D)
	self.slider_change()

    def tr_smooth_path(self):
	self.do_learn=False
	if self.interp_flag == "linear":
	    self.linear_path_smooth()
	if self.interp_flag =="cubic":
	    self.cubic_path_smooth()

        print "Smooth Path"

    def linear_path_smooth(self):
	step_thresh = .02
	step_size = .02
	#theta_def = [-1.7,-1.22,0,-1.7]
	theta_def = [-1.7,-.5,0,-1.0]

	new_waypoints = []
	for pt in self.wayPoint_list:
	    point_waypoints = []
            theta_def[0] = pt[0]
	    current_pt = [theta_def[0], theta_def[1], theta_def[2], theta_def[3]]
	    for j in [0,3,2,1]:
	        while(1):
	            point_waypoints.append([current_pt[0], current_pt[1],current_pt[2],current_pt[3]])
		    error = current_pt[j] - pt[j]
		    change = step_size
		    if math.fabs(error) > 20 * step_size:
			change *= 15
                    if error > step_thresh:
		        current_pt[j]-= change
	            elif error < -1 * step_thresh:
		        current_pt[j] += change
	            else:
		        break	
	    
	    new_waypoints += point_waypoints + point_waypoints[::-1]
	self.wayPoint_list = new_waypoints
	
     
    def cubic_path_smooth(self):
	# implement method in spong 5.5
	theta_def = [-1.7,-0.5,0,-1.0]
	SPEED_DEF = 0.01
	SPEED_INT = 0.5
	SPEED_PT = 0.001 #1.0/1023.0
	time_step = 3
	time_divisions = 24

	destination_waypoints = []
	for pt in self.wayPoint_list:
	    safe_pos = [pt[0], theta_def[1], theta_def[2], theta_def[3], SPEED_DEF]
	    intermediate = [pt[0], 
			    (theta_def[1] + pt[1]) / 2, 
			    (theta_def[2] + pt[2]) / 2, 
		            (theta_def[3] + pt[3]) / 2, 
			    SPEED_INT] 
	    pt = [pt[0], pt[1], pt[2], pt[3], SPEED_PT]
	    destination_waypoints.append(safe_pos)
	    #destination_waypoints.append(intermediate)
	    destination_waypoints.append(pt)
	    #destination_waypoints.append(intermediate)
	    destination_waypoints.append(safe_pos)
	
	# now we have the target and default pts as our path; we want to use the cubic polynomial to 
	# interpolate points and velocities in between these 
	waypoints = []
	velocities = []
	for i in range(len(destination_waypoints)):
	    waypoint_temp = [[0, 0, 0, 0] for x in range(time_divisions + 1)]
	    velocity_temp = [[0, 0, 0, 0] for x in range(time_divisions + 1)]
	    for joint in [0, 1, 2, 3]:
		qo = destination_waypoints[i][joint]
		vo = destination_waypoints[i][4]
		if (len(destination_waypoints) > i+1):
		    qf = destination_waypoints[i + 1][joint]
		    vf = destination_waypoints[i + 1][4]
		else:
		    break
	        a0 = qo
	        a1 = vo
	        a2 = (3 * (qf - qo) - (2 * vo + vf) * time_step) / time_step ** 2 
 		a3 = (2 * (qo - qf) + (vo + vf) * time_step) / time_step ** 3
		
		for k in range(time_divisions + 1):
		    t = k * time_step / float(time_divisions)
                    waypoint_temp[k][joint] = a0 + a1 * t + a2 * t ** 2 + a3 * t ** 3
		    velocity_temp[k][joint] = a1 + 2 * a2 * t + 3 * a3 * t ** 2
		    velocity_temp[k][joint] = math.fabs(velocity_temp[k][joint])

	    if (sum([sum(i) for i in waypoint_temp]) != 0):
	        waypoints.extend(waypoint_temp)
	        velocities.extend(velocity_temp)

	self.wayPoint_list = waypoints
	self.velocity_list = velocities

    def tr_playback(self):
        print "Save and Playback"
	self.save_plan()
        self.ui.sldrMaxTorque.setValue(45)
	self.doPlayback = True
	self.do_learn=False

    def save_plan(self):
	if len(self.wayPoint_list)==0:
		return
	print "Saving waypoint list"
	self.f_gt_waypoints = open('gt_waypoints.csv','w')
	self.f_gt_velocity = open('gt_velocity.csv','w')
	np.savetxt(self.f_gt_waypoints, self.wayPoint_list, delimiter=',')
	np.savetxt(self.f_gt_velocity, self.velocity_list, delimiter=',')
	self.f_gt_waypoints.close()
	self.f_gt_velocity.close()

    def load_plan(self):
	# load in the most current waypoints plan
	self.f_gt_waypoints = open('gt_waypoints.csv','r')
	self.f_gt_velocity = open('gt_velocity.csv','r')
	print "Loading in most recently saved waypoint list"
	self.wayPoint_list = np.genfromtxt(self.f_gt_waypoints, delimiter=',').tolist()
	self.velocity_list = np.genfromtxt(self.f_gt_velocity, delimiter=',').tolist()
	self.f_gt_waypoints.close()
	self.f_gt_velocity.close()

    def playback(self):
		
	# begin a Proportional controller to move current joint angles to the 
	# desired joint angle
	print "Len: ", len(self.wayPoint_list)
	if (len(self.wayPoint_list) == 0):
	    self.doPlayback = False
	    return
	pt = self.wayPoint_list[0]

        # for each waypoint, we use a proportional controller to bring the actual joint angle to the desired joint angle
        max_err = 0
	if len(self.velocity_list) > 0:
	    vel = self.velocity_list[0]
        for i in range(4):
	    if len(self.velocity_list) > 0:
                self.rex.speed[i] = vel[i]
            err = pt[i] - self.rex.joint_angles_fb[i]
            if math.fabs(err) > max_err:
                max_err = math.fabs(err)
        if max_err < WPT_THRES:
	    print "Desired waypoint reached"
	    self.wayPoint_list.remove(pt)
	    if len(self.velocity_list) > 0:
	        self.velocity_list.remove(vel)

	else:
	    print max_err

        self.ui.sldrBase.setValue(pt[0] * R2D)
        self.ui.sldrShoulder.setValue(pt[1] * R2D)
        self.ui.sldrElbow.setValue(pt[2] * R2D)
        self.ui.sldrWrist.setValue(pt[3] * R2D)
	self.slider_change()

    def forward_kin(self):
	# use dh parameters to calculate end effector tip in world coords
	dh_result = [np.eye(4)]
	
	for i in range(len(LINK_LENGTH)):
	    theta = self.rex.joint_angles_fb[i + 1]
	    dh_array = np.array([[cos(theta), -1*sin(theta), 0, LINK_LENGTH[i] * cos(theta)],
		                 [sin(theta), cos(theta), 0, LINK_LENGTH[i] * sin(theta)],
		                 [0, 0, 1, 0],
		                 [0, 0, 0, 1]])
	    dh_result.append(np.dot(dh_result[i], dh_array))
        
	origin = np.array([0, 0, 0, 1]).T
	effector = np.dot(dh_result[3], origin)
	radial_length = -effector[1]
	base_angle = self.rex.joint_angles_fb[0]
	x = radial_length * cos(base_angle)
	y = radial_length * sin(base_angle)

	effectorJoint = np.dot(dh_result[2], origin)
	radial_length_joint = -effectorJoint[1]
	xJoint = radial_length_joint * cos(base_angle)
	yJoint = radial_length_joint * sin(base_angle)
	effector_delta = effector[0] - effectorJoint[0]
	phi = math.acos(effector_delta / float(LINK_LENGTH[2]))
	wE  = [x - X_OFFSET, y, effector[0] + Z_OFFSET, np.pi - phi]
	self.worldEffector = [wE[1], -1*wE[0], wE[2], wE[3] ] 

    def inv_kin_touch(self, x, y):
	z = 25
	angle = -90
	while (1):
	    print "x = ",x,"y =",y
	    thetas = self.inv_kin(x, y, z, angle)
	    print "thetas: ", thetas, "approach angle: ", angle
	    if thetas[1] != 0 and thetas[2]!=0 and thetas[3]!=0: 
		return thetas
	    elif angle <= -15:
		angle += 5
	    else:
		print "Error: Target (" + str(x) + ", " + str(y) +  ") outside of arm reach."
		return [thetas[0], 0, 0, 0]

    # Approach angle of -90 for pointing directly down	
    def inv_kin(self, x1, y1, z, approach_angle):
	# inverse kinematics for 3R planar arm; based on derivation in 
	# http://www.seas.upenn.edu/~meam520/notes02/IntroRobotKinematics5.pdf
	#x = x1 - X_OFFSET
	z = z - Z_OFFSET
	x = y1 - X_OFFSET
	y = -x1 
	theta_1 = np.arctan2(y, x)
	try:
		effector_radius = math.hypot(x, y)
		effector_height = z

		wrist_vec_height = effector_height - LINK_LENGTH[2] * sin(math.radians(approach_angle)) 
		wrist_vec_radius = effector_radius - LINK_LENGTH[2] * cos(math.radians(approach_angle))
		wrist_vec_magnitude = math.hypot(wrist_vec_height, wrist_vec_radius)

		phi =  np.arctan2(wrist_vec_height, wrist_vec_radius)
		Beta =  math.acos( (LINK_LENGTH[0] ** 2 - LINK_LENGTH[1] ** 2 + wrist_vec_magnitude ** 2) / (2. * LINK_LENGTH[0] * wrist_vec_magnitude) )
		gamma = math.acos( (LINK_LENGTH[1] ** 2 - LINK_LENGTH[0] ** 2 + wrist_vec_magnitude ** 2) / (2. * LINK_LENGTH[1] * wrist_vec_magnitude) )
		delta = math.acos( (LINK_LENGTH[1] ** 2 + LINK_LENGTH[0] ** 2 - wrist_vec_magnitude ** 2) / (2. * LINK_LENGTH[1] * LINK_LENGTH[0]) )

		theta_2 = math.pi/2. - phi - Beta 
		theta_3 = math.pi - delta
		theta_4 = math.pi/2. - theta_2 - theta_3 - math.radians(approach_angle)

		return [theta_1, theta_2, theta_3, theta_4]
	except ValueError as e:
		return [theta_1, 0, 0, 0]

    def def_template(self):
        print "Define Template"
	self.define_template_train = True

    def template_calculate(self):
	print "Received Template Coords:"
	lol(self.template_coords)
	# --- ADDED BY AC NOV 16 ---
	homocoord = np.array([0,0,1])
        transform_mat = np.vstack((self.video.aff_matrix,homocoord))
        world_origin = np.array([0,0,1])
        pixel_origin = np.dot(np.linalg.inv(transform_mat),world_origin)
	#---------------------------
	x_array = [x for [x, y] in self.template_coords]
	y_array = [y for [x, y] in self.template_coords]
	template_box = [min(x_array), max(x_array), min(y_array), max(y_array)]
	board_x_array = [x for [x, y] in self.template_board_coords]
	board_y_array = [y for [x, y] in self.template_board_coords]
	board_box = [min(board_x_array), max(board_x_array), min(board_y_array), max(board_y_array)]
	self.video.generate_template(pixel_origin, template_box, board_box)

    def template_match(self):
        print "Template Match"
	t1 = time.time()
	self.video.template_match()
	t2 = time.time()
	print "Took: ", t2-t1

    def exec_path(self):
        print "Execute Path"
	print "UI Object Coords:"
	lol(self.video.object_coords_UI)
	object_coords_mm = [self.pixel_to_mm(x[0], x[1]) for x in self.video.object_coords_UI]
	print "mm Obkect Coords:"
	lol(object_coords_mm)
	object_coords_angles = [self.inv_kin_touch(x[0], x[1]) for x in object_coords_mm]
	object_coords_waypoints = [[x[0], -x[1], -x[2], -x[3], 0] for x in object_coords_angles]
	for pt in object_coords_waypoints:
	    print "old base: ", pt[0]
	    new_base = pt[0] + np.pi
	    print "intermediate base: ", new_base
	    if (new_base > np.pi):
	        new_base -= 2 * np.pi
	    print "wrap-controlled base:", new_base
	    pt[0] = new_base
	
	object_coords_waypoints = sorted(object_coords_waypoints) 
	lol(object_coords_waypoints)
	self.wayPoint_list = [[y[0], y[1], y[2], y[3], y[4]] for y in object_coords_waypoints]
	self.tr_smooth_path()
        for i in range(4):
            self.velocity_list[0][i] = 50
	self.tr_playback()

    def record(self):
	if self.record_init_files ==0:
		self.record_init()
	log_data = [time.time()]
	
	log_data.extend(self.rex.joint_angles_fb)

	for i in range(4):
		log_data.extend([self.worldEffector[i]])
	
	log_data.extend(self.rex.joint_angles)
	
	log_data.extend(self.rex.speed)

	log_data.append(self.ui.sldrBase.value())
        log_data.append(self.ui.sldrShoulder.value())
        log_data.append(self.ui.sldrElbow.value())
        log_data.append(self.ui.sldrWrist.value())

	log_data.extend(self.rex.speed_fb)

	fp = open(self.f_joint_feedback,'a')
	writer = csv.writer(fp)
	writer.writerow(log_data)	
	fp.close()


    def record_init(self):
	print "Initializing recording"
	self.f_joint_feedback = 'log.csv'
	#self.f_joint_feedback_timestamp = 'timestamp_executed_waypoints.csv'
	print "created/opened appropriate files"
	self.record_init_files=1
	row = ['Timestamp',
	       'JointAngles[0]', 'JointAngles[1]', 'JointAngles[2]', 'JointAngles[3]',
	       'Effector[x]', 'Effector[y]', 'Effector[z]','Effector[phi]',
	       'Waypoint[0]', 'Waypoint[1]', 'Waypoint[2]', 'Waypoint[3]',
	       'Desired Vel[0]', 'Desired Vel[1]', 'Desired Vel[2]', 'Desired Vel[3]',
	       'Desired Joint Position[0]', 'Desired Joint Position[1]', 'Desired Joint Position[2]', 'Desired Joint Position[3]',
		'Actual Velocity[0]','Actual Velocity[1]','Actual Velocity[2]','Actual Velocity[3]']
	fp = open(self.f_joint_feedback,'a')
	writer = csv.writer(fp)
	writer.writerow(row)	
	fp.close()

def lol(s):
    for e in s:
	print "    ", e

def main():
    app = QtGui.QApplication(sys.argv)
    ex = Gui()
    ex.show()
    sys.exit(app.exec_())
 
if __name__ == '__main__':
    main()
