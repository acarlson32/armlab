import cv2
import numpy as np
from PyQt4 import QtGui, QtCore, Qt
import matplotlib.pyplot as plt
from scipy import signal
from scipy.ndimage.interpolation import zoom
import time
import math

GREY_THRES = 80

class Video():
    def __init__(self,capture):
        self.capture = capture
        self.capture.set(3, 1280)
        self.capture.set(4, 960)
        #self.capture.set(5, 10) 		# FPS
        self.currentFrame=np.array([])
	self.currentGreyThreshold = np.array([])

        """ 
        Affine Calibration Variables 
        Currently only takes three points: center of arm and two adjacent 
        corners of the base board
        Note that OpenCV requires float32 arrays
        """
        self.aff_npoints = 8                                    # for each black mark on board; start top right
        nval = 250.
        self.real_coord = np.float32([[nval, nval], [nval, 0.], [nval,-nval],[0.,-nval], [-nval,-nval], [-nval,0.],[-nval,nval],[0.,nval]])
        self.mouse_coord = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0]])      
        self.mouse_click_id = 0;
        self.aff_flag = 0;
        self.aff_matrix = np.float32((2,3))

	# Camera Calibration
	self.isCalibrated = False
	self.camera_matrx = [] 
	self.new_camera_matrix = []
	self.dist_coefs = []
	self.template = np.array([])
	self.board = np.array([])
	self.board_offset = [0, 0]
	self.object_coords = []
	self.object_coords_UI = []

    
    def captureNextFrame(self):
        """                      
        Capture frame, convert to RGB, and return opencv image      
        """
        ret, frame=self.capture.read()
        
	if(ret==True):
            rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
	
	    if (self.isCalibrated):
            	rgb_frame = cv2.undistort(rgb_frame, self.camera_matrix, 
		    	                  self.dist_coefs, None, self.new_camera_matrix)
	    # Rotate
	    angle = 90 
	    rows,cols = frame.shape
	    M = cv2.getRotationMatrix2D((cols/2,rows/2), angle, 1)
	    rgb_frame = cv2.warpAffine(rgb_frame, M, (cols, rows))

	    # Resize
	    width = 900
	    height = 900
	    x_start = int(self.capture.get(3) / 2 - width / 2)
	    x_end = int(self.capture.get(3) / 2 + width / 2)
	    y_start = int(self.capture.get(4) / 2 - height / 2)
   	    y_end = int(self.capture.get(4) / 2 + height / 2)
	    rgb_frame = rgb_frame[y_start:y_end, x_start:x_end].copy()

	    self.currentFrame = rgb_frame

	    # Create thresholded image
	    grey_frame = cv2.cvtColor(rgb_frame, cv2.COLOR_BGR2GRAY)
	    #idx_obj = grey_frame[:,:] < GREY_THRES
	    #idx_free = grey_frame[:,:] >= GREY_THRES
	    #grey_frame[idx_obj] = 255
	    #grey_frame[idx_free] = 0
	    env_background_average = np.mean(grey_frame)
	    #print "average background: ",math.ceil( env_background_average) 
	    self.currentGreyThreshold = grey_frame -math.ceil( env_background_average)


    def convertFrameThres(self):
        """ Converts frame to format suitable for QtGui  """
        try:
	    frame = self.currentGreyThreshold
            height,width = frame.shape[:2]
	    for obj in self.object_coords:
		offset = 5
	        frame[(obj[1] - offset):(obj[1] + offset),
	    	      (obj[0] - offset):(obj[0] + offset)] = 120
            img=QtGui.QImage(frame,
                              width,
                              height,
                              QtGui.QImage.Format_Indexed8) # Indexed8 or RGB888
            img=QtGui.QPixmap.fromImage(img)
            self.previousFrame = self.currentFrame
            return img
        except:
            return None


    def convertFrameRgb(self):
        """ Converts frame to format suitable for QtGui  """
        try:
	    frame = self.currentFrame
            height,width = frame.shape[:2]
	    for obj in self.object_coords:
		offset =10
	        frame[(obj[1] - offset):(obj[1] + offset),
	    	      (obj[0] - offset):(obj[0] + offset)] = [255, 0, 0]
            img=QtGui.QImage(frame,
                              width,
                              height,
                              QtGui.QImage.Format_RGB888) # Indexed8 or RGB888
            img=QtGui.QPixmap.fromImage(img)
            self.previousFrame = self.currentFrame
            return img
        except:
            return None


    def loadCalibration(self):
        """
        Load calibration from file and applies to the image
        """
        w = int(self.capture.get(3))
        h = int(self.capture.get(4))
	try:
            self.camera_matrix = np.genfromtxt('camera_matrix_calib.csv',delimiter=',')
            self.dist_coefs = np.genfromtxt('dist_coeffs_calib.csv',delimiter=',')
            self.new_camera_matrix = np.genfromtxt('new_camera_matrix_calib.csv',delimiter=',')
	    self.aff_matrix = np.genfromtxt('affine_calib.csv',delimiter=',').reshape((2,3))
	    self.aff_flag = 2
	except:
	    pass
	self.isCalibrated = True


    def generate_template(self,pixel_origin, template_box, board_box):
	print "Generating template..."
        height, width = self.currentGreyThreshold.shape[:2]
	print "Grey Threshold Frame Size, ", width, "x", height
	preview_width = 640
	preview_height = 480
	print "Preview/UI Frame Size, ", preview_width, "x", preview_height
        template_x_min = int(template_box[0] * width / preview_width)
        template_x_max = int(template_box[1] * width / preview_width)
        template_y_min = int(template_box[2] * height / preview_height)
        template_y_max = int(template_box[3] * height / preview_height)
        board_x_min = int(board_box[0] * width / preview_width)
        board_x_max = int(board_box[1] * width / preview_width)
        board_y_min = int(board_box[2] * height / preview_height)
        board_y_max = int(board_box[3] * height / preview_height)
        
	board_origin_x = int(pixel_origin[0] * width / preview_width)
	board_origin_y = int(pixel_origin[1] * height /preview_height)
        print " origin xy: ", board_origin_x, board_origin_y	
	board_width = 320 # (board_x_max - board_x_min)/2.
	board_height = 320#(board_y_max - board_y_min)/2.

	bxmin = board_origin_x - board_width
	bxmax = board_origin_x + board_width
	bymin = board_origin_y - board_height
	bymax =  board_origin_y + board_height
	print " x min: ", bxmin, "x max: ", bxmax," y min:", bymin, "y max: ", bymax
	self.board = self.currentGreyThreshold[bymin:bymax, bxmin:bxmax].copy()
	#self.board = self.currentGreyThreshold
	self.board_offset = [bxmin, bymin]
	self.template = self.currentGreyThreshold[template_y_min:template_y_max, template_x_min:template_x_max].copy()
	remove_arm_offset = 80
        self.board[int(board_origin_x - bxmin - remove_arm_offset):int(board_origin_x - bxmin + remove_arm_offset), int(board_origin_y - bymin - remove_arm_offset):int(board_origin_y - bymin + remove_arm_offset)] = 0

	#plt.imshow(self.template)
	#plt.show()

	#plt.imshow(self.board)
	#plt.show()

	"""
	# ORIGINAL CODE	
	template_x_min = int(template_box[0] * width / preview_width)
	template_x_max = int(template_box[1] * width / preview_width)
	template_y_min = int(template_box[2] * height / preview_height)
	template_y_max = int(template_box[3] * height / preview_height)
	board_x_min = int(board_box[0] * width / preview_width)
	board_x_max = int(board_box[1] * width / preview_width)
	board_y_min = int(board_box[2] * height / preview_height)
	board_y_max = int(board_box[3] * height / preview_height)
	self.board_offset = [board_x_min, board_y_min]
	
	#self.board_offset = [board_box[0], board_box[2]]
	print "Board Offset, UI Frame:    ", [board_box[0], board_box[2]]
	print "Board Offset, Array Frame: ", self.board_offset
	self.template = self.currentGreyThreshold[template_y_min:template_y_max, template_x_min:template_x_max].copy()

	self.board = self.currentGreyThreshold[board_y_min:board_y_max, board_x_min:board_x_max].copy()
	
	h, w = self.board.shape[:2]
	offset = 50
	self.board[int(h/2 - offset):int(h/2 + offset), int(w/2 - offset):int(w/2 + offset)] = 0
	#plt.imshow(self.template)
	#plt.show()
	#plt.imshow(self.board)
	#plt.show()
	"""

    def template_match(self):
	CORRELATION_THRES = 80 # 30 for q=8, 50 for q=6, 160 for q=4
	q = 7
	#down_template = signal.decimate(signal.decimate(self.template, q, ftype='fir', axis=0), q, ftype='fir', axis=1)
	#down_frame = signal.decimate(signal.decimate(self.board, q, ftype='fir', axis=0), q, ftype='fir', axis=1)
	down_template = zoom(self.template, 1/float(q), order=0)
	down_board = zoom(self.board, 1/float(q), order=0)
	template_h, template_w = down_template.shape[:2]
	#plt.imshow(down_template)
	#plt.show()
	#plt.imshow(down_board)
	#plt.show()
	print "Board Size:       ", self.board.shape[:2]
	print "DownBoard Size:   ", down_board.shape[:2]
	print "Template Size:    ", self.template.shape[:2]
	print "DownTemplate Size:", down_template.shape[:2]
	#convolution = signal.convolve2d(down_template, down_board)
	convolution = self.convolve(down_template, down_board)
	height, width = convolution.shape[:2]
	print "Convolution Size: ", height, "x", width
	#plt.imshow(convolution)
	#plt.show()

	target_coords = []
	last_idx = 0
	while (1):
	    max_idx = np.ndarray.argmax(convolution)
	    max_val = convolution.flatten()[max_idx]
	    max_x = max_idx % width
	    max_y = int(max_idx / width)
	    print "max: ", max_val, " at: ", max_idx,"or", max_x, max_y
	    #plt.imshow(convolution)
	    #plt.show()
	    #time.sleep(5)
	    if (max_val < CORRELATION_THRES or last_idx == max_idx):
		break
	    last_idx = max_idx
	    #plt.imshow(convolution)
	    #plt.show()

	    template_h, template_w = down_template.shape[:2]
	    convolution[(max_y - template_h / 2):(max_y + template_h / 2),
	    	        (max_x - template_w / 2):(max_x + template_w / 2)] = 0
	    target_coords.append([max_x, max_y])
	
	 
 	print "Target Coordinates, Board Frame: ", target_coords
	BIAS = int(q / 2)
	global_target_coords = [[q * (x + template_w / 2) + self.board_offset[0] + BIAS, q * (y + template_h / 2) + self.board_offset[1] + BIAS] for [x, y] in target_coords]
	print "Global Coordinates, Array Frame: ", global_target_coords
	print "Global Coordinates, UI Frame:    ", [[x * 640 / 900, y * 480 / 900] for [x, y] in global_target_coords]
	self.object_coords = global_target_coords
	self.object_coords_UI = [[x * 640 / 900, y * 480 / 900] for [x, y] in global_target_coords]

    def convolve(self,template, board):
    	STEP_SIZE = 1
        board_h, board_w = board.shape[:2]
        template_h, template_w = template.shape[:2]
	print "board Width: ", board_w, " board height: ", board_h
	conv_matrix = np.zeros((board_h,board_w))
	highest_max=0
    	for i in range(0, board_w - template_w, STEP_SIZE):
		for j in range(0, board_h - template_h, STEP_SIZE):
	            submatrix = board[j:j+template_h, i:i+template_w ]
		    conv_matrix[j,i] = 1. / ( sum(sum(np.square(template - submatrix))) + 1.)
		    if conv_matrix[j,i] > highest_max:
			    highest_max = conv_matrix[j,i]

	return (255*conv_matrix)/highest_max
   
